<?php
namespace Holt\Tests;

use Silex\WebTestCase;

class BaseTest extends WebTestCase
{
    public function createApplication()
    {
        $app = require __DIR__.'/../../tests/testinit.php';
        $app['debug'] = true;
        unset($app['exception_handler']);

        return $app;
    }

    public function testFooBar()
    {

        echo "Test ok";
        $this->assertNotNull($this->app);
    }
}
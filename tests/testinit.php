<?php
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : gethostname()));
require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../config/common.php';
require_once __DIR__.'/../config/'.APPLICATION_ENV.'.php';
require_once __DIR__.'/../src/app.php';
require_once __DIR__.'/../src/routes.php';

return $app;
var app = angular.module("HoltApp", []);

app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
});

app.controller("TestController", ['$scope', function($scope){
  $scope.ok_js = 'fa-check';
}]);
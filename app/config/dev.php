<?php

$config['debug'] = true;

// DB connection info
$config['dbs.options']['main']['dbname']   = 'projectdb_dev';
$config['dbs.options']['main']['host']     = 'localhost';
$config['dbs.options']['main']['user']     = 'DEV_USER';
$config['dbs.options']['main']['password'] = 'DEV_PWD';

return $config;

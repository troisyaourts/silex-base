<?php
use Holt\Application;
use Silex\Provider;
use Monolog\Handler\ChromePHPHandler;
use Monolog\Handler\RotatingFileHandler;
use Security\UserProvider;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

/*
*  Init app and services providers
*/
$app            = new Holt\Application();
$app['debug']   = $config['debug'];
$app['config']  = $config;

$app->register(new Provider\SessionServiceProvider());
$app->register(new Provider\ServiceControllerServiceProvider());

// Twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
  'twig.options' => array('strict_variables' => true, 'cache' => ($app['debug'] ? false : __DIR__.'/../cache')),
  'twig.path' => __DIR__.'/views',
));

// Gestion des logs
$app->register(new Silex\Provider\MonologServiceProvider(), array(
  'monolog.logfile' => __DIR__.'/../logs/all.log',
  'monolog.name' => 'boostrap'
));

// Cache
$app->register(new Provider\HttpCacheServiceProvider(), array(
  'http_cache.cache_dir' => __DIR__.'/../cache/'
));

$app->register(new Provider\DoctrineServiceProvider(), array('dbs.options' => $config['dbs.options']));
$app->register(new Alpha\Bridge\Silex2\ServiceProvider());

// Login
$app->register(new Silex\Provider\SecurityServiceProvider());
$app['security.firewalls'] = array(
  'login' => array(
    'pattern' => '^/admin/login$',
  ),
  'admin' => array(
    'pattern' => '^/(admin|ajax)',
    'form' => array('login_path' => '/admin/login', 'check_path' => '/admin/login_check'),
    'logout' => array('logout_path' => '/admin/logout'),
    // admin / foo
    'users' => array(
      'admin' => array('ROLE_ADMIN', '$2y$10$3i9/lVd8UOFIJ6PAMFt8gu3/r5g0qeCJvoSlLCsvMTythye19F77a'),
    ),
    // 'users' => $app->share(function () use ($app) {
    //   return new UserProvider($app['db']);
    // }),
  ),
);

$app['security.role_hierarchy'] = array(
    'ROLE_ADMIN' => array('ROLE_MODERATEUR'),
);

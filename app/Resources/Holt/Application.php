<?php

namespace Holt;

class Application extends \Silex\Application{
  use \Silex\Application\TwigTrait;
  use \Silex\Application\SecurityTrait;
  use \Silex\Application\FormTrait;
  use \Silex\Application\UrlGeneratorTrait;
  //use Application\SwiftmailerTrait;
  use \Silex\Application\MonologTrait;
  use \Silex\Application\TranslationTrait;
}
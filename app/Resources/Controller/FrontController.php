<?php

namespace Controller;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class FrontController
{
  public function indexAction(Application $app, Request $request) {

    try{
      $ok_db = $app['db']->executeQuery('SELECT 1'); 
    }catch(\Exception $e){
      $ok_db = false;
    }
    

    $response = new Response($app['twig']->render('index.html.twig',compact("ok_db")));
    return $response;
  }
}


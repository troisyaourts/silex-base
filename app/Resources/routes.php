<?php
use Symfony\Component\HttpFoundation\Request;

/*
 *  FRONT
 */
$app->get('/', 'Controller\FrontController::indexAction')->bind('home');

/*
 *  ADMIN
 */
$admin = $app['controllers_factory'];
$admin->match('/', 'Controller\AdminController::indexAction')->bind('admin_home');
$admin->match('/login', function(Request $request) use ($app) {
  return $app['twig']->render('admin/login.html.twig', array(
    'error'         => $app['security.last_error']($request),
    'last_username' => $app['session']->get('_security.last_username'),
  ));
});
$app->mount('/admin', $admin);

/*
 * AJAX
 */
$ajax = $app['controllers_factory'];
$ajax->match('/', 'Controller\AjaxController::indexAction')->bind('ajax');
$app->mount('/ajax', $ajax);

<?php

class Migration_0001{

  function updateSchema($schema){
    
    $table = $schema->createTable("user");
    $table->addColumn("id", "integer", ["unsigned" => true, "autoincrement" => true]);

    $table->addColumn("email", "string", ["length" => 255,"notnull"=>false]);
    $table->addColumn("password", "string", ["length" => 255,"notnull"=>false]);

    $table->addColumn("pseudo", "string", ["length" => 255,"notnull"=>false]);
    
    $table->addColumn("roles", "text", ["notnull" => false]);

    $table->addColumn("enabled", "integer", ["unsigned" => true, "notnull" => false, "default" => 1]);
    
    $table->addColumn("created_at", "datetime", ["notnull"=>false]);
    $table->addColumn("updated_at", "datetime", ["notnull"=>false]);

    $table->setPrimaryKey(["id"]);

    return $schema;
  }

}
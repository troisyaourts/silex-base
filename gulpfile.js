var gulp = require('gulp');
var $    = require('gulp-load-plugins')();
var merge = require('merge-stream');

/***
 *  CHOOSE HERE WHAT YOU WANT
 ***/

 var app_name = "holt";

var frontSassPaths = [
  'bower_components/foundation-sites/scss',
  'bower_components/motion-ui/src',
];

var frontCssPaths = [
];

var frontJsPaths = [
    'bower_components/angular/angular.js',
    'assets/scripts/*.js',
    'assets/scripts/front/*.js'
];

var adminSassPaths = [
];

var adminCssPaths = [
    'bower_components/bootstrap/dist/css/bootstrap.css',
    'bower_components/bootstrap/dist/css/bootstrap-theme.css',
    'bower_components/admin-lte/dist/css/AdminLTE.css',
    'bower_components/admin-lte/dist/css/skins/_all-skins.css',
    'bower_components/font-awesome/css/font-awesome.css',
];

var adminJsPaths = [
    'bower_components/jquery/dist/jquery.js',
    'bower_components/bootstrap/dist/js/bootstrap.js',
    'bower_components/admin-lte/dist/js/app.js',
    'assets/scripts/*.js',
    'assets/scripts/admin/*.js'
];




gulp.task('images', function(){
  gulp.src('assets/images/**/*')
    .pipe($.cache($.imagemin({ optimizationLevel: 3, progressive: true, interlaced: false })))
    .pipe(gulp.dest('web/dist/img/'));
});

gulp.task('icons', function() { 
    return gulp.src([
      'bower_components/font-awesome/fonts/**.*'
      ]) 
        .pipe(gulp.dest('./web/dist/fonts')); 
});



function compute_style(output, sassEntryPoint, sassPaths, cssPaths){
  var sassStream = gulp.src(sassEntryPoint)
                    .pipe($.sass({
                        includePaths: sassPaths,
                        outputStyle: 'compressed'
                  }).on('error', $.sass.logError));
  var cssStream  = gulp.src(cssPaths);
  return merge(cssStream, sassStream)
    .pipe($.concat(output+'.css'))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(gulp.dest('./web/dist/css'))
    //minify ?
}

function compute_script(output, jsPaths){
  return gulp.src(jsPaths)
  .pipe($.plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe($.concat(output+'.js'))
    // .pipe(gulp.dest('js'))
    .pipe(gulp.dest('./web/dist/js'))
    .pipe($.rename({suffix: '.min'}))
    .pipe($.uglify())
    // .pipe(gulp.dest('js'))
    .pipe(gulp.dest('./web/dist/js'))
}

gulp.task('styles', function(){
  compute_style(app_name, 'assets/scss/front/app.scss', frontSassPaths, frontCssPaths);
});

gulp.task('styles-admin', function(){
  compute_style('admin', 'assets/scss/admin/styles.scss', adminSassPaths, adminCssPaths);
});

gulp.task('scripts', function(){
  compute_script(app_name, frontJsPaths);
});

gulp.task('scripts-admin', function(){
  compute_script('admin', adminJsPaths);
});

gulp.task('default', ['styles', 'styles-admin', 'scripts', 'scripts-admin'], function(){
  gulp.watch("assets/scss/front/*.scss", ['styles']);
  gulp.watch("assets/scss/admin/*.scss", ['styles-admin']);
  gulp.watch("assets/scripts/front/*.js", ['scripts']);
  gulp.watch("assets/scripts/admin/*.js", ['scripts-admin']);
});
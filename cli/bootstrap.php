<?php
date_default_timezone_set('Europe/Paris'); 

// Get the application_env (local, dev, prod etc...) and put it in APPLICATION_ENV and $appEnv
define("APPLICATION_ENV_FILE", __DIR__.'/application_env.php');
if(!is_readable(APPLICATION_ENV_FILE)) 
	die("ERROR: no " . APPLICATION_ENV_FILE . " file found in the cli directory\n". 
			"Create one with the following lines :\n" . "<?php\n" . "return \"local\";\n");

$appEnv = include(APPLICATION_ENV_FILE);
putenv("APPLICATION_ENV=$appEnv");

// get the config file (local.php or dev.php or prod.php) and put it in $config
include(__DIR__."/../app/config/common.php");
define("CONFIG_FILE",  __DIR__."/../app/config/" . $appEnv . ".php");
if(!is_readable(CONFIG_FILE)) {
	echo "ERROR: no " . CONFIG_FILE . " file found\n";
	exit();	
}
$config = include(CONFIG_FILE);

// init Silex
$loader = require_once __DIR__.'/../vendor/autoload.php';

$loader = require_once __DIR__.'/../app/Resources/app.php';

echo "SILEX INIT OK in $appEnv environment \n";
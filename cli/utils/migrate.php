<?php
/**
 * Détecte les changements en base de donnée
 */
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

$console->register('db:migrate')
->setDescription('Lance les migrations de schéma de base de donnée')
->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {

  $db = $app['db'];
  $platform = new Doctrine\DBAL\Platforms\MySqlPlatform();

  $executeQueries = false;
  if ($input->getOption('execute')) {
      $executeQueries = true;
  }


  $dir = __DIR__."/../../app/migrations/";
  $handle = opendir($dir);
  if(!$handle) {
    echo "ERROR : unable to open $dir";
    continue;
  }

  $files = [];
  //Iterate over files
  while (false !== ($entry = readdir($handle))) {
    if(is_file($dir.$entry)) {
      $files[] = $entry;
    }
  }

  if(empty($files)){
    $output->writeln("Nothing to do !");
    return;
  }

  //Sort them in the right order
  usort($files, function($a, $b){
    list($ida, $namea) = explode("_", $a, 2);
    list($idb, $nameb) = explode("_", $b, 2);
    return (int)$idb <= (int)$ida;
  });

  $schema = new \Doctrine\DBAL\Schema\Schema();
  
  //Construct ideal schema
  foreach($files as $f){
    list($mgid, $name) = explode("_", $f, 2);
    $output->writeln("Executing migration ".$mgid." : ".$name);
    include($dir.$f);

    $migclass = "Migration_".$mgid;
    $migration = new $migclass;
    $schema = $migration->updateSchema($schema);
  }

  $sm = $db->getSchemaManager();
  $idealTables = $schema->getTables();
  $realTableNames = $sm->listTableNames( );

  foreach($idealTables as $idealTable){
    //Create table if not in realTableNames (existing tables)
    //or Make diff from existing
    $idealTableName = $idealTable->getName();
    if(in_array($idealTableName, $realTableNames)){
      $realTable = $sm->listTableDetails($idealTableName);
      $comparator = new \Doctrine\DBAL\Schema\Comparator();
      $tableDiff = $comparator->diffTable($realTable, $idealTable);
      if($tableDiff=== false){
        continue;
      }
      $mig_queries = $platform->getAlterTableSQL($tableDiff);
    }else{
      $mig_queries = $platform->getCreateTableSQL($idealTable);
    }

    //get the queries and execute them if force
    foreach ($mig_queries as $q) {
      $output->writeln($q);
      if($executeQueries){
        $db->query($q);
      }
    }
  }
  
})->addOption('execute',null,InputOption::VALUE_NONE,'Execute the sql queries');


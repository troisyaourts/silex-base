<?php

defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : gethostname()));

// silex core
require_once __DIR__.'/../vendor/autoload.php';

// common config 
require_once __DIR__.'/../app/config/common.php';

// environement config (DBs ...)
require_once __DIR__.'/../app/config/'.APPLICATION_ENV.'.php';

// init service providers
require_once __DIR__.'/../app/Resources/app.php';

// define routes
require_once __DIR__.'/../app/Resources/routes.php';

// let's go
$app->run();
